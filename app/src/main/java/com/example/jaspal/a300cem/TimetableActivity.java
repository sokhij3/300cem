package com.example.jaspal.a300cem;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.icu.util.Calendar;
import android.nfc.Tag;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class TimetableActivity extends AppCompatActivity {

    DatabaseHelper timetableDB;
    Button addButton, voiceButton;
    EditText inputModule;
    private TextView voiceInput;
    Spinner daySpinner, timeSpinner, buildingSpinner;
    private final int SPEECH_RECOGNITION_CODE = 1;

    /*Creating the arrays to be put into the spinners for input. */
    String[] day = new String[]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    String[] time = new String[]{"09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00"};
    String[] building = new String[]{"Alan Berry", "Alma", "Armstrong Siddeley",
            "Bugati", "Engineering", "Ellen Terry", "Library", "George Elliot",
            "Graham Sutherland", "The Hub", "Jaguar", "James Starley", "Maurice Foss",
            "Richard Crossman", "Sir John Lang", "Sir William Lyons", "Student Centre", "William Morris", "Sports Centre"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable);

        timetableDB = new DatabaseHelper(this);

        inputModule = (EditText) findViewById(R.id.inputModule); //identifying the widgets
        voiceInput = (TextView) findViewById(R.id.voiceInput);

        daySpinner = (Spinner) findViewById(R.id.daySpinner);
        /*Creates an array adapter from my string array and specifies the layout.*/
        ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, day);
        /*Applies the adapter to the spinner. */
        daySpinner.setAdapter(dayAdapter);

        timeSpinner = (Spinner) findViewById(R.id.timeSpinner);
        ArrayAdapter<String> timeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, time);
        timeSpinner.setAdapter(timeAdapter);

        buildingSpinner = (Spinner) findViewById(R.id.buildingSpinner);
        ArrayAdapter<String> buildingAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, building);
        buildingSpinner.setAdapter(buildingAdapter);

        addButton = (Button) findViewById(R.id.addButton);

        voiceButton = (Button) findViewById(R.id.voiceButton);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                startSpeechToText();
            }
        });

        addData();
        //addVoiceData();
    }

    public void addData(){
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*Gets the inputs and converts them to strings. */
                String Module = inputModule.getText().toString();
                String Day = daySpinner.getSelectedItem().toString();
                String Time = timeSpinner.getSelectedItem().toString();
                String Building = buildingSpinner.getSelectedItem().toString();


                boolean insertData = timetableDB.addData(Module, Day, Time, Building);

                if(insertData == true){
                    Toast.makeText(TimetableActivity.this, "Timetable session saved!", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(TimetableActivity.this, "Data entered incorrectly!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /*code to input data using voice input
     *
     * !!!Code if faulty however and not finished!!!
     */
    private void startSpeechToText() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Speak something...");
        try {
            startActivityForResult(intent, SPEECH_RECOGNITION_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Speech recognition is not supported in this device.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SPEECH_RECOGNITION_CODE: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = result.get(0);
                    voiceInput.setText(text);
                }
                break;
            }
        }
    }


}

