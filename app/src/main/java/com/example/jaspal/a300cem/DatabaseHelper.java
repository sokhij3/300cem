package com.example.jaspal.a300cem;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.IntegerRes;

import static android.R.attr.name;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    public static final String DATABASE_NAME = "Timetable.db";
    public static final String TABLE_NAME = "Timetable";
    public static final String Column1 = "TimetableID";
    public static final String Column2 = "Module";
    public static final String Column3 = "Day";
    public static final String Column4 = "Time";
    public static final String Column5 = "Building";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                "MODULE TEXT, DAY TEXT, TIME TEXT, BUILDING TEXT)";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP IF TABLE EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean addData(String Module, String Day, String Time, String Building){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Column2, Module);
        contentValues.put(Column3, Day);
        contentValues.put(Column4, Time);
        contentValues.put(Column5, Building);

        long result = db.insert(TABLE_NAME, null, contentValues);

        if(result == -1){
            return false;
        }else{
            return true;
        }
    }

    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getTimetableID(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + Column1 + "FROM" + TABLE_NAME + "WHERE" + Column2 + "='" + name + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public void deleteRow(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        //return db.delete(TABLE_NAME, Column2 + "=" + id, null) > 0;
        db.execSQL("DELETE FROM " + TABLE_NAME + " WHERE ID = " + id + "");
    }

}