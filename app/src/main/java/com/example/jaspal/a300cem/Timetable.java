package com.example.jaspal.a300cem;

/**
 * Created by Jaspal on 12/12/2017.
 */

public class Timetable {
    private int timetableID;
    private String moduleName;
    private String date;
    private String time;
    private String building;

    public Timetable(int timetableID, String moduleName, String date, String time, String building) {
        this.timetableID = timetableID;
        this.moduleName = moduleName;
        this.date = date;
        this.time = time;
        this.building = building;
    }

    public int getTimetableID() {
        return timetableID;
    }

    public void setTimetableID(int timetableID) {
        this.timetableID = timetableID;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }
}



