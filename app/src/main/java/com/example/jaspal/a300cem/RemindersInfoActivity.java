package com.example.jaspal.a300cem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class RemindersInfoActivity extends AppCompatActivity {

    private static final String TAG = "ReminderInfoActivity";
    DatabaseHelper aDatabaseHelper;
    private String selectedName;
    private int selectedID;
    private EditText editableItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminders_info);
        editableItem = (EditText) findViewById(R.id.editableItem);
        aDatabaseHelper = new DatabaseHelper(this);

        Intent receivedIntent = getIntent();
        selectedID = receivedIntent.getIntExtra("id",-1);
        selectedName = receivedIntent.getStringExtra("name");
        editableItem.setText(selectedName);
    }
}
