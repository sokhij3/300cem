package com.example.jaspal.a300cem;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    /*variables used for map functionality*/
    public static int REQUEST_LOCATION = 1;
    static public Location mLastLocation;
    static private OnCurrentLocationChangeListener mOnCurrentLocationChangeListener;
    protected TextView mOutput;
    protected Button mLocateButton;
    protected Button mMapButton;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Geocoder mGeocoder;

    Button notifyButton;

    /*variables used for voice input and playback*/
    public static int REQUEST_SPEECH = 1;
    File aAudioFile;
    private MediaRecorder mediaRecorder = null;
    private MediaPlayer mediaPlayer = null;
    boolean recording = false;

    public static void setOnCurrentLocationChangeListener(OnCurrentLocationChangeListener
                                                                  mOnCurrentLocationChangeListener) {
        MainActivity.mOnCurrentLocationChangeListener = mOnCurrentLocationChangeListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //code for daily notifications
        findViewById(R.id.notifyButton).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                Calendar calendar = Calendar.getInstance();
                //Notification set to go off at 08:30am daily if turned on
                calendar.set(Calendar.HOUR_OF_DAY, 17);
                calendar.set(Calendar.MINUTE, 15);
                calendar.set(Calendar.SECOND, 10);
                Intent intent = new Intent(getApplicationContext(), Notification.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                /*The notification is set to go off every day at the given if turned on. It also wakes the phone up if it is closed due to RTC_WAKEUP*/
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
                Toast.makeText(MainActivity.this, "Daily notifications turned on", Toast.LENGTH_LONG).show();
            }
        });


        //code for maps
        mLocateButton = (Button) findViewById(R.id.locate);
        mMapButton = (Button) findViewById(R.id.mapButton);
        mOutput = (TextView) findViewById((R.id.output));

        //these are not enabled as we need user permission to access location first.
        mLocateButton.setEnabled(false);
        mMapButton.setEnabled(false);
        mOutput.setText("");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        //code to create file to store audio recorded.
        try {
            aAudioFile = createFile(this, "audioReminders");
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onConnected(@Nullable Bundle connectionHint) {
        Log.d("xxxx", "90");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            Log.d("xxxx", "93");

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            Log.d("xxxx", "99");
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //granting permission to access location.
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onConnected(null);
            }
        }
        //granting permission to record audio
        else if (requestCode == REQUEST_SPEECH) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted
                recordAudio();
            } else {
                // permission denied
                Toast.makeText(this, "Permissions Denied to record audio", Toast.LENGTH_LONG).show();
            }
            return;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mOnCurrentLocationChangeListener != null) {
            mOnCurrentLocationChangeListener.onCurrentLocationChange(location);
        }
    }

    //used to enable widgets in runtime when toggle is clicked.
    public void onStartClicked(View v) {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            mLocateButton.setEnabled(true);
            mMapButton.setEnabled(true);
            mOutput.setText("You can now find your location");
        } else {
            mGoogleApiClient.disconnect();
            mLocateButton.setEnabled(false);
            mMapButton.setEnabled(false);
            mOutput.setText("Location services stopped.");
        }
    }

    public void onLocateClicked(View v) {
        mGeocoder = new Geocoder(this);
        try {
            List<Address> addresses = mGeocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);

            if (addresses.size() == 1) {
                Address address = addresses.get(0);
                StringBuilder addressLines = new StringBuilder();
                if (address.getMaxAddressLineIndex() > 0) {
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        addressLines.append(address.getAddressLine(i) + "\n");
                    }
                } else {
                    addressLines.append(address.getAddressLine(0));
                }
                mOutput.setText(addressLines);
            } else {
                mOutput.setText("More then on address returned.");
            }
        } catch (Exception e) {
            mOutput.setText("Failed to get Location.");
        }
    }

    public interface OnCurrentLocationChangeListener {
        void onCurrentLocationChange(Location location);
    }

    /*Code to record a reminder and play it back
     *includes the permissions for RECORD_AUDIO
     */
    private void requestAudioPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            //used to give user option to allow permissions to record audio
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {
                Toast.makeText(this, "Grant permissions to record reminder", Toast.LENGTH_LONG).show();

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        REQUEST_SPEECH);

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        REQUEST_SPEECH);
            }
        }
        //If permission is granted, audio recording is allowed
        else if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {

            recordAudio();
        }
    }

    private static File createFile(Context context, String audioName) throws IOException {
        File storage = context.getExternalFilesDir(Environment.DIRECTORY_PODCASTS);
        File audio = File.createTempFile(audioName, ".3gp", storage);
    return audio;
    }

    public void startRecording(View view){
        requestAudioPermissions();
    }

    private void recordAudio() {
        if (mediaRecorder == null) {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setOutputFile(aAudioFile.getAbsolutePath());
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        }

        if (!recording) {
            try {
                mediaRecorder.prepare();
                mediaRecorder.start();
                recording = true;
            } catch (IOException e) {
                Log.e("Audio", "prepare() failed");
            }
        } else if (recording) {
            recording = false;
            stopRecording();
        }
    }

    public void stopRecording() {
        if (mediaRecorder != null) {
            mediaRecorder.stop();
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    public void startPlaying(View view) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(aAudioFile.getAbsolutePath());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            Log.e("Audio", "prepare() failed");
        }
    }

    public void stopPlaying() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    public void onDestroy() {

        if (mediaPlayer != null) {
            stopPlaying();
        }
        if (mediaRecorder != null) {
            stopRecording();
        }
        super.onDestroy();
    }

    public void goToTimetableActivity(View view) {
        startActivity(new Intent(MainActivity.this, TimetableActivity.class));
    }

    public void goToRemindersActivity(View view) {
        startActivity(new Intent(MainActivity.this, RemindersActivity.class));
    }

    public void goToMapActivity(View view) {
        startActivity(new Intent(MainActivity.this, MapActivity.class));
    }

}
