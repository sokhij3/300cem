package com.example.jaspal.a300cem;

import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.jaspal.a300cem.R.styleable.View;

public class RemindersActivity extends AppCompatActivity {

    private static final String TAG = "RemindersActivity";
    DatabaseHelper timetableDB;
    private ListView aListView;

    Button delButton;
    EditText idInput;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminders);
        aListView = (ListView) findViewById(R.id.reminderList);
        delButton = (Button) findViewById(R.id.deleteButton);
        idInput = (EditText) findViewById(R.id.inputID);
        timetableDB = new DatabaseHelper(this);

        addToListView();
        deleteData();
    }

    private void addToListView() {
        Log.d(TAG, "addToListView: Displaying data in the ListView.");

        //getting the data that is input and adding it to a list
        Cursor data = timetableDB.getData();
        final ArrayList<String> listData = new ArrayList<>();
        while(data.moveToNext()){
            //get the first value from the database and add it to the list
            listData.add(data.getString(1));
        }
        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        aListView.setAdapter(adapter);
    }

    public void deleteData(){
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Takes input from edit text and inputs it into the deleteRow function in the database helper class.*/
                String temp = idInput.getText().toString();
                int num = temp.length();
                if(num > 0){
                    timetableDB.deleteRow(temp);
                    Toast.makeText(RemindersActivity.this, "Deleted!", Toast.LENGTH_LONG).show();

                }else{
                    Toast.makeText(RemindersActivity.this, "You must enter an id!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}