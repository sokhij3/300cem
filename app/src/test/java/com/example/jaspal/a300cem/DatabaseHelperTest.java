package com.example.jaspal.a300cem;

import android.content.ContentValues;
import android.test.InstrumentationTestCase;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.*;
import android.database.Cursor;
import android.test.InstrumentationTestCase;
import android.util.*;

/**
 * Created by Jaspal on 12/12/2017.
 */
public class DatabaseHelperTest extends InstrumentationTestCase{

    DatabaseHelperTest databaseManager;
    SQLiteDatabase write;
    SQLiteDatabase read;
    ContentValues contentValues;

    @Before
    public void setup() throws Exception {
        super.setUp();

        databaseManager = DatabaseHelperTest.getInstance(getInstrumentation().getTargetContext());

        write = databaseManager.getWritableDatabase();
        read = databaseManager.getReadableDatabase();
        contentValues = new ContentValues();
    }

    private SQLiteDatabase getWritableDatabase() {
         long result = da.insert("test", "test2", "test3");
    }

    private static DatabaseHelperTest getInstance(Context targetContext) {
          return null;
    }


    @Test
    public void addData() throws Exception {
        String Column2 = "340CT";
        String Column3 = "Monday";
        String Column4 = "12:00";
        String Column5 = "Engineering";

        String Module = null;
        String Day = null;
        String Time = null;
        String Building = null;

        ContentValues contentValues = new ContentValues();
        contentValues.put(Column2, Module);
        contentValues.put(Column3, Day);
        contentValues.put(Column4, Time);
        contentValues.put(Column5, Building);

        assertThat(Module, is(Column2));
        assertThat(Day, is(Column3));
        assertThat(Time, is(Column4));
        assertThat(Building, is(Column5));

    }

    @Test
    public void getData() throws Exception {

    }

    @Test
    public void deleteRow() throws Exception {

    }

}